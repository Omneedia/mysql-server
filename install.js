const OS = require('os');
const path = require('path');
const fs = require('fs-extra');
const axios = require('axios').default;
const chalk = require('chalk');
const ora = require('ora');
const { exec } = require('child_process');

var oram;

var pro = 'default';

var userdir = require('os').homedir() + '/oa-cli';

var userdirdata = userdir + '/db';
userdirdata += '/' + pro;
var data = userdirdata + '/data';

var INSTALL = [
  'https://github.com/Omneedia/mysql-server/raw/master/mysql-server-win64.zip',
  'https://github.com/Omneedia/mysql-server/raw/master/mysql-server-win32.zip',
  'https://github.com/Omneedia/mysql-server/raw/master/mysql-server-macos.zip',
];

var isWin = /^win/.test(process.platform);

var sep = '/';

var arch = OS.arch();

if (OS.platform() == 'darwin') var url = INSTALL[2];
if (OS.platform() == 'win32') {
  if (OS.arch() == 'x64') var url = INSTALL[0];
  else var url = INSTALL[1];
}

var filename = path.resolve(
  __dirname + sep + url.substr(url.lastIndexOf('/') + 1, url.length)
);

var Request = require('axios');

var _progress = require('cli-progress');

var bar1 = new _progress.Bar(
  {
    format: chalk.cyan(
      '  [ {bar} ] ' + chalk.bold('{percentage}%') + ' | ETA: {eta}s'
    ),
  },
  _progress.Presets.shades_classic
);

var progress = 0;

function init_db() {
  fs.stat(data + '/auto.cnf', function (e, r) {
    if (e) {
      var progress = ora(chalk.bold.cyan('Initializing MySQL server')).start();

      exec(
        __dirname +
          '/mysql/bin/mysqld --defaults-file="' +
          userdirdata +
          '/my.ini" -b "' +
          __dirname +
          '/mysql" --datadir="' +
          data +
          '" --initialize-insecure',
        function () {
          progress.succeed(chalk.green('OK ') + ' MySQL server installed.');
          var msg =
            '\n' +
            chalk.green('  MySQL installed.') +
            " - Don't forget to start server with " +
            chalk.bold('oa db start') +
            '\n';
          console.log(msg);
        }
      );
    } else {
      var msg =
        '\n' +
        chalk.green('  MySQL installed.') +
        " - Don't forget to start server with " +
        chalk.bold('oa db start') +
        '\n';
      console.log(msg);
    }
  });
}

function conf_db() {
  var progress = ora(chalk.bold.cyan('configuring mySQL'));
  var myini = [
    '[mysqld]',
    'sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES',
    'max_allowed_packet=160M',
    'innodb_force_recovery=0',
    'port=3306',
    'federated',
    'show_compatibility_56 = ON',
    'server-id = 1',
  ];
  fs.writeFile(userdirdata + '/my.ini', myini.join('\r\n'), function () {
    progress.succeed(
      chalk.green('OK ') + ' MySQL default configuration applied'
    );
    init_db();
  });
}

function uncompress() {
  console.log(' ');
  var progress = ora(chalk.cyan.bold('  extracting MySQL')).start();
  var unzip = require('unzip-stream');
  fs.createReadStream(filename)
    .pipe(
      unzip.Extract({
        path: path.dirname(filename),
      })
    )
    .on('error', function (e) {
      progress.fail('archive is uncomplete or damaged...');
      fs.unlink(filename, download);
    })
    .on('close', function () {
      fs.unlink(filename, function () {
        progress.succeed(chalk.green('OK ') + ' archive extracted');
        var arch = OS.arch();
        if (OS.platform() == 'darwin') {
          fs.copy(__dirname + '/macos', __dirname + '/mysql', function () {
            fs.rmdir(__dirname + '/macos', { recursive: true }, function () {
              fs.chmod(__dirname + '/mysql/bin/mysqld', 755, function () {
                fs.mkdir(data, { recursive: true }, conf_db);
              });
            });
          });
        }
        if (OS.platform() == 'win32') {
          if (OS.arch() == 'x64') {
            fs.copy(__dirname + '/win64', __dirname + '/mysql', function () {
              fs.rmdir(__dirname + '/win64', { recursive: true }, function () {
                fs.mkdir(data, { recursive: true }, conf_db);
              });
            });
          } else {
            fs.copy(__dirname + '/win32', __dirname + '/mysql', function () {
              fs.rmdir(__dirname + '/win64', { recursive: true }, function () {
                fs.mkdir(data, { recursive: true }, conf_db);
              });
            });
          }
        }
      });
    });
}

async function download(cb) {
  const { data, headers } = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  });
  const totalLength = headers['content-length'];
  oram.succeed('Downloading MySQL');
  console.log(' ');
  bar1.start(parseInt(totalLength), 0, {
    speed: 'N/A',
    barCompleteChar: '\u2588',
    barIncompleteChar: '\u2591',
  });
  const writer = fs.createWriteStream(filename);

  data.on('data', function (chunk) {
    progress += chunk.length;
    bar1.update(progress, {});
  });
  data.on('end', function () {
    bar1.update(totalLength, {
      speed: 0,
    });
    bar1.stop();
    uncompress();
  });
  data.pipe(writer);
}
oram = ora('Initializing transfert...').start();
download();
